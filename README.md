This mod replaces every players entire build (except charms, consumables, and money) with new items at customization intervals. 

The three options for these intervals are: 
Every Floor  [maximum chaos  :)]
Every Zone  [new build at start of X-1]
Once a Run [get a free, randomized build, at 1-1]

Can also customize the chance of getting an item in a given slot. With this set to 100% you will always have an item in all 7 slots, but you can change this to lower values to have less consistently strong builds.