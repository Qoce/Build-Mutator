local event = require "necro.event.Event"
local ecs = require "system.game.Entities"
local playerSystem = require "necro.game.character.Player"
local inventory = require "necro.game.item.Inventory"
local itemGeneration = require "necro.game.item.ItemGeneration"
local settings = require "necro.config.Settings"
local currentLevel = require "necro.game.level.CurrentLevel"
local enum = require "system.utils.Enum"
local rng = require "necro.game.system.RNG"

local rngChannel = 11037

local newBuildTrigger = enum.sequence {
    EVERY_FLOOR = 1,
    EVERY_ZONE = 2,
    ONCE_A_RUN = 3,
}
local mode = enum.sequence{
    TRANSMUTE = 1,
    CONJURE = 2,
}

HowOften = settings.shared.enum {
    name = "New Build",
    enum = newBuildTrigger,
}
ChanceOfItem = settings.shared.percent {
    name = "Item Chance",
    default = 1,
    maximum = 1,
    minimum = 0,
    step = 0.05
}
Mode = settings.shared.enum{
    name = "Mutation Mode",
    enum = mode,
    default = mode.CONJURE,
}

local function generateItemInSlot(player, slot)
    local itemType = itemGeneration.weightedChoice(rngChannel, "secret", 0, slot)
    inventory.grant(itemType, player)
end

local function cappedExpDist(p, max)
    local n = 0
    while n < max and rng.roll(p, rngChannel) do
        n = n + 1
    end
    return  n
end

local function clearSlot(player, slot)
    for _,item in ipairs(inventory.getItemsInSlot(player, slot)) do
        inventory.destroy(ecs.getEntityByID(item))
    end
end

local function transmuteItemsInSlot(player, slot)
    local count = #inventory.getItemsInSlot(player,slot) or 0
    clearSlot(player,slot)
    for _ = 1, count do
        generateItemInSlot(player, slot)
    end
end

local function conjureItemsInSlot(player, slot)
    clearSlot(player, slot)
    local numberToGenerate
    if slot == "spell" then
        numberToGenerate = cappedExpDist(0.4 * ChanceOfItem, 2)
    elseif slot == "misc" then
        numberToGenerate = cappedExpDist(0.6 * ChanceOfItem, 8)
    else
        numberToGenerate = 1
    end
    for _ = 1, numberToGenerate do
        if slot == "weapon" or rng.roll(ChanceOfItem, rngChannel) then
            generateItemInSlot(player, slot)
        elseif slot == "shovel" then
            inventory.grant("ShovelBasic", player)
        end
    end
end

event.levelLoad.add("randomizeBuilds", {order = "entities", sequence = 1}, function(ev)
    if (HowOften == newBuildTrigger.EVERY_ZONE or HowOften == newBuildTrigger.ONCE_A_RUN) and currentLevel.getFloor() > 1 then
        return
    end
    if HowOften == newBuildTrigger.ONCE_A_RUN and currentLevel.getDepth() > 1 then
        return
    end
    for _, p in ipairs(playerSystem.getPlayerEntities()) do
        p.descentDamageImmunity.active = true
        local slots = {"head", "shovel", "feet", "weapon", "body", "torch", "ring", "action", "spell", "misc"}
        for i = 1,#slots do
            if not inventory.isCursedSlot(p, slots[i]) then
                if Mode == mode.CONJURE then
                    conjureItemsInSlot(p, slots[i])
                else
                    transmuteItemsInSlot(p, slots[i])
                end
            end
        end
        p.descentDamageImmunity.active = false
    end
end)